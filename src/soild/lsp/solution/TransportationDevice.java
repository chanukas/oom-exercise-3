package soild.lsp.solution;

import soild.lsp.violation.Engine;

public class TransportationDevice {
    String name;

    double speed;

    public String getName() {
        return name;
    }

    public double getSpeed() {
        return speed;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }


}
