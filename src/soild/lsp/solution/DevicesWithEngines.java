package soild.lsp.solution;

import soild.lsp.violation.Engine;

public class DevicesWithEngines  extends TransportationDevice{

    Engine engine;

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    void startEngine() {}
}
