package soild.lsp.violation;

public class TransportationDevice {
    String name;

    double speed;

    public String getName() {
        return name;
    }

    public double getSpeed() {
        return speed;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    Engine engine;

    void startEngine() {}
}
