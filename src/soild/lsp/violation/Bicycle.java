package soild.lsp.violation;

public class Bicycle extends TransportationDevice{

    // there is a problem here. bicycle is definitely a transportation device. however, it does not have an engine
    // the method startEngine() cannot be implemented.
    void startEngine() {}
}
