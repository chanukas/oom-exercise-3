package soild.dip.violation;



// this code will work for now, but if we wanted to change computer class this will require refactoring the LapTopComputer class
// so we can solve this by introducing abstraction.

public class LapTopComputer {

    private final Computer computer;

    LapTopComputer(Computer c){
        computer = c;
    }

    public void powerOn() {
        computer.powerOn();
    }
}
