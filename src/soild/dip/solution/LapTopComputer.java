package soild.dip.solution;


/// ///(DIP) states that we should depend on abstractions (interfaces and abstract classes) instead of concrete implementations (classes).
//// The abstractions should not depend on details instead, the details should depend on abstractions.


public class LapTopComputer {

    private final Computer computer;

    LapTopComputer(Computer c){
        computer = c;
    }

    public void powerOn() {
        computer.powerOn();
    }
}
