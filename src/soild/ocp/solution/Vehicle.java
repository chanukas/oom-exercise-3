package soild.ocp.solution;


///// Software components should be open for extension, but not for modification.

class Vehicle {

    double value = 0;

    public double calculateValue() {
        return 0;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
