package soild.ocp.violation;


public class VehicleCalculations {
    public double calculateValue(Vehicle v) {
        if (v instanceof Car) {
            return v.getValue() * 0.8;
        }else if (v instanceof Bike){
            return v.getValue() * 0.5;
        }else{
            return 0;
        }
    }
}
